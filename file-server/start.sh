#!/bin/sh

if [ $# -lt 1 ] ; then
	echo "Usage: ./start.sh <PORT>"
	exit 1
fi

TEST_PORT=$1
CONTAINER_NAME="nginx_fs"

docker run --name $CONTAINER_NAME -v "$PWD/files:/usr/share/nginx/html:ro" -p $TEST_PORT:80 -d nginx
